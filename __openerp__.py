{
    'name': 'To-Do demonstrate types of views',
    'description': 'Manage    your    personal    Tasks    with    this    module.',
    'author': 'Gajanan Kathar',
    'depends': ['mail'],
    'data' : [
              'views/todo_views.xml',
              'views/todo_actions.xml',
              ],
    'application': True,
}