#    -*-    coding:    utf-8    -*-
from openerp import models, fields, api
class TodoTask(models.Model):
    _name = 'todo.task'
    
    # Calling methods on header buttons on click
    # @api.one works on the current record in form view and if it is called for multiple records Api can handlle that and trigger the method for each records in list view.
    @api.one
    def do_toggle_done(self):
        self.is_done = not self.is_done
        return True
    
    # In @api.multi self represents the recordset.It can contain single record, when used from form view and multiple records when used from list view.
    @api.multi
    def do_clear_done(self):
        done_recs = self.search([('is_done','=',True)])
        print"\n\nDone records ",done_recs,"\n\n"
        done_recs.write({'is_active':False})
        return True
    
    name = fields.Char('Description', required=True)
    is_done = fields.Boolean('Done?')
    is_active = fields.Boolean('Active?', default=True)